<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile()
    {
        return $this->hasOne('\App\UserProfile');
    }

    public function words()
    {
        return $this->belongsToMany('App\Word')
        ->withTimestamps()
        ->withPivot('state')
        ;
    }

    public function todaywords($state=1)
    {
        $carbon = \Carbon\Carbon::now();
        $today_start = $carbon->startOfDay();
        $today_end = $carbon->copy()->endOfDay();

        return $this->words()
        ->wherePivot('created_at','>=',$today_start)
        ->wherePivot('created_at','<=',$today_end)
        ->wherePivot("state",$state)
        ;
    }

    public function getWordIdArray()
    {
        $return = [];
        $words = $this->words;
        foreach ($words as $word) {
            $return[] = $word->id;
        }

        return $return;
    }
}
