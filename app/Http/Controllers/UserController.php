<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\UserProfile;
use Illuminate\Support\Str;

class UserController extends Controller
{

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:5'
        ]);

        if ($validator->fails()) {
            return [
                'status'=>false,
                'errors'=>$validator->errors()
            ];
        }

        $user = new User;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->token = Str::random(60);
        $user->save();

        $user_profile = new UserProfile;
        $user_profile->level = $request->level;
        $user_profile->user_id = $user->id;
        $user_profile->save();

        return [
            'status'=>true,
            'user'=>$user
        ];
    }
}
