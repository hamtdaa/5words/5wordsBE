<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\User;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $user;

    public function init($request)
    {
        $token = $request->bearerToken();

        $check_token = $this->checkToken($token);

        if ($check_token['status'] == false) {
            return $check_token;
        }

        $check_user = $this->checkUser($token);
        if ($check_user['status'] == false) {
            return $check_user;
        }
        $this->user = $check_user['user'];
        return [
            'status' => true,
        ];
    }

    private function checkToken($token)
    {
        if (empty($token)) {
            return [
                'status' => false,
                'message' => 'Token not found',
            ];
        }
        return [
            'status' => true,
        ];
    }

    private function checkUser($token)
    {
        $user = User::where('token', $token)->first();
        if (empty($user)) {
            return [
                'status' => false,
                'message' => 'User not found',
            ];
        }
        return [
            'status' => true,
            'user' => $user,
        ];
    }
}
