<?php
Route::group(['prefix' => 'api'], function () {
    Route::group(['prefix' => 'user'], function () {
        Route::post('register', 'UserController@register');
        Route::post('login', 'UserController@login');
        Route::post('logout', 'UserController@logout');
    });
    // TODO middleware token
    Route::group(['prefix' => 'word'], function () {
        Route::post('today', 'WordController@today');
        Route::post('week', 'WordController@week');
        Route::post('all', 'WordController@all');
    });
});

Route::any('{id?}', function ($id) {
    return [
        'status' => false,
        'message' => 'Not Found',
    ];
});
